You need to execute a bunch of git commands. Save all your commands in a text file called `git_drill_part_1.txt`

1. Create a directory called `git_sample_project` and `cd` to `git_sample_project`

```
mkdir git_sample_project
cd git_sample_project
```

2. Initialize a git repo.
```
git init
```

3. Create the following files `a.txt`, `b.txt`, `c.txt`
```
touch a.txt
touch b.txt
touch c.txt
```
4. Add some arbitrary content to the above files.
```
echo "this is a's text file." >> a.txt
echo "this is b's text file." >> b.txt
echo "this is c's text file." >> c.txt
```

5. Add `a.txt` and `b.txt` to the staging area
```
git add a.txt b.txt
```

6. Run git status. Understand what it says.
```
git status
```
7. Commit `a.txt` and `b.txt` with the message "Add a.txt and b.txt"
```
git commit -m "Add a.txt and b.txt"
```
8. Run git status. Understand what it says
```
git status
```
9. Run git log
```
git log
```
10. Add and Commit `c.txt`
```
git add c.txt
git commit -m 'Add c.txt' 
```
11. Create a project on GitLab.
```
git config --global user.name "Mohan KUmar Sajja"
git config --global user.email "moka1309@gmail.com"
git init

```
12. Push your code to GitLab
```
git remote add origin https://gitlab.com/mohan.sajja/git-sample-project.git
git push -u origin master
```
13. Clone your project in another directory called `git_sample_project_2`
```
git clone https://gitlab.com/mohan.sajja/git-sample-project.git git_sample_project_2
```
14. In `git_sample_project_2`, add a file called `d.txt`
```
cd git_sample_project_2
touch d.txt
```
15. Commit and push `d.txt`
```
git add d.txt
git commit -m "Add d.txt"
git push -u origin master
```
16. cd back to `git_sample_project`
```
cd ..

```
17. Pull the changes from GitLab
```
git pull
git pull origin master #to get back again master privilages
```
18. Copy `git_drill_part_1.txt` to `git_sample_project`. Commit and Push it on GitLab
```
cd ..
cp git_drill_part_1.txt git_sample_project/
git add git_drill_part_1.txt
git commit -m "Add git_drill_part_1.txt"
git push -u origin master
```
19. Copy `cli_drill_part_1.txt` to `git_sample_project`. Commit and Push it on GitLab
```
cp cli_drill_part_1.txt git_sample_project/
git add cli_drill_part_1.txt
git commit -m "Add cli_drill_part_1.txt"
git push -u origin master
```

20. Add santu@mountblue.io and pramod@mountblue.io as "Developers" to your project