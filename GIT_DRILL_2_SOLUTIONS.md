# Drills

## Local commands

### Part 1 - Merging

* Create a directory called `git_exercise`.
```
mkdir git_exercise
```

* `cd` to `git_exercise`.
```
cd git_exercise
```

* Initialize a `git` repository at `git_exercise`
```
git config --global user.name "Mohan KUmar Sajja"
git config --global user.email "moka1309@gmail.com"
git init
```
* Create a file called `one.py` and add the following contents to the file:

```
touch one.py
```


```
print('one')
x = [1, 2, 3]
y = [4, 5, 6]
```
```
echo "print('one')
x = [1, 2, 3]
y = [4, 5, 6]" > one.py

```


* Commit `one.py` with the message "Initial message"
```
git add .
git commit m "Initial message"
```

* Add another file called `two.py` with the following contents

```
print("two")
```
```
touch two.py
echo "print("two")" > two.py

```
* Execute `git status`. Understand each line printed by the command

```
git status
```

* Create a new branch called `feature-2`
```
git branch feature-2
git checkout feature-2
```

* Commit `two.py` in `feature-2`
```
git add .
git commit -m 'Add two.py by feature-2 branch'
```
* Go to `master` branch. Merge `feature-2`
```
git checkout master
git merge feature-2
```
* Create a branch named `feature-3`. 
```
git branch feature-3
git checkout feature-3
```
* Create a file called `three.py` with the following contents

```
print('three')
```
```
touch three.py
echo "print('three')" > three.py
```

* Create a file called `README.md` with the following contents

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
```
```
touch README.md
echo "## Hello, world!

To run 'feature-3', execute 'python3 three.py'"
```

* Commit `three.py` and `README.md`
```
git add .
git commit -m 'Add three.py and README.md by feature-3'
```
* Switch to master branch. (DON'T merge `feature-3` now)
```
git checkout master
```
* Create a new branch named `feature-4`
```
git branch feature-4
git checkout feature-4
```

* Create a file called `four.py` with the following contents

```
print("four")
```
```
touch four.py
echo "print('four')" > four.py
```
* Create a file called `README.md` with the following contents

```
## Hello, world!

To run `feature-4`, execute `python3 four.py`
```
```
touch README.md

echo "## Hello, world!

To run 'feature-4', execute 'python3 four.py'"
```
* Commit `four.py` and `README.md`
```
git add .
git commit -m "Add four.py and README.md by feature-4"
```
* Switch to master branch
```
git checkout master
```

* Merge `feature-3`
```
git merge feature-3
```
* Merge `feature-4`. Fix the merge conflicts
```
git merge feature-4
```

`README.md` must contain:

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
To run `feature-4`, execute `python3 four.py`
```

* Run git status
```
git status
```
* Commit the files that need aren't committed yet.
```
git add -A
git commit -m "Add README.md"
```

### Part 2 - Rebase

* Create a branch named `feature-5`. 
```
git branch feature-5
```
* Create a file called `five.py` with the following contents

```
print("five")
```
```
touch five.py
echo "print('five')" > five.py
```
* Edit `README.md` so that it has the following contents

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
To run `feature-4`, execute `python3 four.py`
To run `feature-5`, execute `python3 five.py`
```
```
vi README.md
```
* Commit `five.py` and `README.md`
```
git add .
git commit -m "Add five.py and edit file README.md"
```
* Switch to master branch. (DON'T merge `feature-5` now)
`git checkout master`
* Create a new branch named `feature-6`
```
git branch feature-6
git checkout feature-6
```
* Create a file called `six.py` with the following contents

```
print("six")
```
```
touch six.py
echo 'print("six")' > six.py
```

* Create a file called `README.md` with the following contents

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
To run `feature-4`, execute `python3 four.py`
To run `feature-6`, execute `python3 six.py`
```
```
touch README.md
vi README.md
```
* Commit `six.py` and `README.md`
```
git add six.py
git commit -m "Add six.py and README.md"
```
* Switch to master branch
```
git checkout master
```
* Merge `feature-5`
`git merge feature-5`
* Switch to `feature-6` branch
`git checkout feature-6`
* Rebase `feature-6` from `master`
```
git rebase master
```
* Fix the merge conflicts

`README.md` must contain:

```
## Hello, world!

To run `feature-3`, execute `python3 three.py`
To run `feature-4`, execute `python3 four.py`
To run `feature-5`, execute `python3 five.py`
To run `feature-6`, execute `python3 six.py`
```

* Run git status
```
git status
```
* Commit the files that need aren't committed yet.
```
git add .
git commit -m "Edit README.md"
```
* Switch to `master`
```
git checkout master
```
* Merge `feature-6`
```git merge feature-6
```
* Create a repo on Gitlab.

* Set `origin` to your Gitlab repo.
```git remote add origin git@gitlab.com:mohankumar.sajja/git-exercise
```

* Push `master`, `feature-2`, `feature-3`, `feature-4`, `feature-5`,
`feature-6` to `origin`
```
git push -u origin --all
```
* Send me a link to the your Gitlab repo.

